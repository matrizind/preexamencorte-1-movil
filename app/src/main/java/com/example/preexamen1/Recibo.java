package com.example.preexamen1;

import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.io.Serializable;

public class Recibo implements Serializable {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public Recibo(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public Recibo() {
        this.numRecibo = 0;
        this.nombre = "";
        this.horasTrabNormal = 0.0f;
        this.horasTrabExtras = 0.0f;
        this.puesto = 0;
        this.impuestoPorc = 0.0f;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    //metodos
    public float calcularSubtotal(){
        float pagoBase = 200.0f;

        switch (puesto) {
            case 1:
                pagoBase += pagoBase * 0.20;
                break;
            case 2:
                pagoBase += pagoBase * 0.50;
                break;
            case 3:
                pagoBase += pagoBase * 1.00;
                break;
            default:

                break;
        }

        return ((pagoBase * horasTrabNormal) + (horasTrabExtras * pagoBase) *2);

    }

    public float calcularImpuesto(){
        impuestoPorc = calcularSubtotal();
        return (float) (impuestoPorc * 0.16);
    }

    public float calcularTotal(){
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }
}
