package com.example.preexamen1;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboActivity extends AppCompatActivity {

    private TextView txtUsuario,txtSubtotal, txtImpuesto, txtTotal;
    private EditText txtRecibo, txtNombre, txtHorasNormal, txtHorasExtras;
    private RadioGroup rdGroup;
    private RadioButton rdbAuxiliar, rdbAlbañil, rdbIng;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo);
        iniciarComponentes();
        //Recibir la variable de cliente desde ingresaCotizacion.java
        Intent intent = getIntent();
        String cliente = intent.getStringExtra("cliente");
        //Asignar el texto al Textview
        if (cliente != null) {
            txtUsuario.setText(cliente);
        } else {
            Toast.makeText(getApplicationContext(), "No se recibió el usuario", Toast.LENGTH_SHORT).show();
        }
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtHorasNormal.getText().toString().matches("") ||
                        txtHorasExtras.getText().toString().matches("") ||
                        rdGroup.getCheckedRadioButtonId() == -1){
                    Toast.makeText(getApplicationContext(),"FALTAN AGREGAR DATOS", Toast.LENGTH_SHORT).show();
                }
                else{
                    calcular();
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtRecibo.setText("");
                txtNombre.setText("");
                txtHorasNormal.setText("");
                txtHorasExtras.setText("");
                rdGroup.clearCheck();
                txtSubtotal.setText("");
                txtImpuesto.setText("");
                txtTotal.setText("");
            }
        });


    }



    public void iniciarComponentes(){
        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        txtSubtotal = (TextView) findViewById(R.id.txtSubtotal);
        txtImpuesto = (TextView) findViewById(R.id.txtImpuesto);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtRecibo = (EditText) findViewById(R.id.txtRecibo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtHorasNormal = (EditText) findViewById(R.id.txtHorasNormal);
        txtHorasExtras = (EditText) findViewById(R.id.txtHorasExtras);
        rdGroup = (RadioGroup) findViewById(R.id.rdGroup);
        rdbAuxiliar = (RadioButton) findViewById(R.id.rdbAuxiliar);
        rdbAlbañil = (RadioButton) findViewById(R.id.rdbAlbañil);
        rdbIng = (RadioButton) findViewById(R.id.rdbIng);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }

    private void calcular() {

        int numRecibo = Integer.parseInt(txtRecibo.getText().toString());
        String nombre = txtNombre.getText().toString();
        float horasTrabNormal = Float.parseFloat(txtHorasNormal.getText().toString());
        float horasTrabExtras = Float.parseFloat(txtHorasExtras.getText().toString());
        int puesto = 0;

        if(rdGroup.getCheckedRadioButtonId() == R.id.rdbAuxiliar){
            puesto = 1;
        }
        else if(rdGroup.getCheckedRadioButtonId() == R.id.rdbAlbañil){
            puesto = 2;
        }
        else if(rdGroup.getCheckedRadioButtonId() == R.id.rdbIng){
            puesto = 3;
        }
        float impuestoPorc = 16.0f; // Porcentaje de impuesto fijo

        // Crear una instancia de Recibo
        Recibo recibo = new Recibo(numRecibo, nombre, horasTrabNormal, horasTrabExtras, puesto, impuestoPorc);

        // Calcular subtotal, impuesto y total
        float subtotal = recibo.calcularSubtotal();
        float impuesto = recibo.calcularImpuesto();
        float total = recibo.calcularTotal();

        // Mostrar los resultados en la interfaz de usuario
        txtSubtotal.setText("Subtotal: $" + subtotal);
        txtImpuesto.setText("Impuesto: $" + impuesto);
        txtTotal.setText("Total a pagar: $" + total);
    }

}
